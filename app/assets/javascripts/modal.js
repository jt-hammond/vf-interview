$(function() {
  // Length Needs to Correspond to days and Vice Versa
  // length needs to equal days(food, accomoadation, activities and car)
  let $daysFood = $('#daysFood');
  let $daysCar = $('#daysCar');
  let $daysAccomodations = $('#daysAccomodations');
  let $daysActivities = $('#daysActivities');
  let $whereBody = $('#whereBody');

  $('#nextBtn').click(function(){
    let $length = $('#travelTime');
    let $goalName = $('#goalName');
    let $destination = $('#destinationName');
    let $type = $('#travelType');
    $daysFood.val = $daysCar.val = $daysAccomodations.val = $daysActivities.val = $length;
    $whereBody.addClass("hidden");
    $whereTitle.addClass("hidden")
    $costBody.removeClass("hidden");
    $costTitle.removeClass("hidden");
  });

  $('#nextSecondBtn').click(function(){
    $costBody.addClass("hidden");
    $costTitle.addClass("hidden");
    $summaryBody.removeClass("hidden");
    $summaryTitle.removeClass("hidden")
  });

  $('#doneBtn').click(function(){
    $summaryBody.addClass("hidden");
    $summaryTitle.addClass("hidden");
    $whereBody.removeClass("hidden");
    $whereTitle.removeClass("hidden")
  });

  // Needs to Save Necessary Info and add it to the screen to main screen

  // Needs to move between Modals and titles
  // Needs to advance progress bar
  let $whereTitle = $('#whereTitle');
  let $costTitle = $('#costTitle');
  let $summaryTitle = $('#summaryTitle');

  let $costBody = $('#costBody');
  let $summaryBody = $('#summaryBody');

  $("#modalProgress").css("width", "66%");
  $("#modalProgress").css("width", "100%");

  // Needs to calculate amounts for estimates
  let $amountFlights = $('#amountFlights');
  let $people = $('#peopleFlights');
  let $estimateFlight = $('#estimateFlight');

  let $estimateAccomadation = $('#estimateAccomadation');
  let $amountAccomodations = $('#amountAccomodations');

  let $amountCar = $('#amountCar');
  let $estimateCar = $('#estimateCar');

  let $estimateFood = $('#estimateFood');
  let $amountFood = $('#amountFood');

  let $estimateActivities = $('#estimateActivities');
  let $amountActivities = $('#amountActivities');

  // Needs to calculate total amount
  // total = estimate(flight, activities, car, accomodates, food)
  let $total = $('#total');

  // Needs to use estimate if checked
  // If checked than total is equal to the estimate and estimateCost
  let $estimateGeneral = $('#estimateGeneral');
  let $guessEstimate = $('#guessEstimate');


  // Needs to replace elements in summary page
  let $image = $('#fileUpload');
  let $estimateCost = $('#costEstimate');
  let $date = $('#date');
  let $contribution = $('#contribution');
  let $final = $('#final');


  // Needs to append to the main body
  const $mainBody = $('#main-body');
});
